package main

import (
	"bufio"
	"fmt"
	"math"
	"net"
	"strconv"
	"strings"
)

func check(err error, message string) {
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", message)
}

func IsPrimeSqrt(value int) bool {
	for i := 2; i <= int(math.Floor(math.Sqrt(float64(value)))); i++ {
		if value%i == 0 {
			return false
		}
	}
	return value > 1
}

func return_number(array_elem string) int {
	sum := 0
	for _, i := range array_elem {
		number, eroare := strconv.Atoi(string(i))
		if eroare == nil {
			sum = sum*10 + number
		}
	}
	return sum
}

func checkPrimeNumbers(conn net.Conn, input string) {

	counter := 0
	input = input[:len(input)-2]
	lista := strings.Split(input, " ")
	for _, element := range lista {
		number, _ := strconv.Atoi(string(element))
		if IsPrimeSqrt(number) {
			for number > 0 {
				number = number / 10
				counter = counter + 1
			}

		}
	}
	conn.Write([]byte(strconv.Itoa(counter) + "\n"))
}

func main() {
	ln, err := net.Listen("tcp", ":8080")
	check(err, "Server is ready.")

	for {
		conn, err := ln.Accept()
		check(err, "Accepted connection.")

		go func() {
			buf := bufio.NewReader(conn)

			for {
				list, err := buf.ReadString('\n')

				if err != nil {
					fmt.Printf("Client disconnected.\n")
					break
				}
				if len(list) > 2 || list != "\n" {
					go checkPrimeNumbers(conn, list)
				}
			}
		}()
	}
}
